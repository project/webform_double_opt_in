<?php

namespace Drupal\webform_double_opt_in\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class OptedInEvent.
 *
 * @package Drupal\webform_double_opt_in\Event
 */
class OptedInEvent extends Event {

  /**
   * The Webform ID.
   *
   * @var string
   */
  protected string $webformId;

  /**
   * The Webform submission source entity type.
   *
   * @var string|null
   */
  protected string|null $webformSubmissionSourceEntityType;

  /**
   * The Webform submission source entity ID.
   *
   * @var int|string|null
   */
  protected string|int|null $webformSubmissionSourceEntityId;

  /**
   * The webform submission ID.
   *
   * @var int|string|null
   */
  protected int|string|null $webformSubmissionId;

  /**
   * The opt-in mail address.
   *
   * @var string
   */
  protected string $emailAddress;

  /**
   * OptedInEvent constructor.
   *
   * @param string $webformId
   *   The Webform ID.
   * @param string|null $webformSubmissionSourceEntityType
   *   The Webform submission source entity type.
   * @param int|string|null $webformSubmissionSourceEntityId
   *   The Webform submission source entity ID.
   * @param int|string|null $webformSubmissionId
   *    The Webform submission ID.
   * @param string $emailAddress
   *   The opt-in mail address.
   */
  public function __construct(
    string $webformId,
    string|null $webformSubmissionSourceEntityType,
    int|string|null $webformSubmissionSourceEntityId,
    int|string|null $webformSubmissionId,
    string $emailAddress
  ) {
    $this->webformId = $webformId;
    $this->webformSubmissionSourceEntityType = $webformSubmissionSourceEntityType;
    $this->webformSubmissionSourceEntityId = $webformSubmissionSourceEntityId;
    $this->webformSubmissionId = $webformSubmissionId;
    $this->emailAddress = $emailAddress;
  }

  /**
   * Returns the Webform ID.
   *
   * @return string
   *   The Webform ID.
   */
  public function getWebformId():string {
    return $this->webformId;
  }

  /**
   * Returns the Webform submission source entity type.
   *
   * @return string|null
   *   The type of the submission source entity.
   */
  public function getWebformSubmissionSourceEntityType(): ?string {
    return $this->webformSubmissionSourceEntityType;
  }

  /**
   * Returns the Webform submission source entity ID.
   *
   * @return int|string|null
   *   The ID of the submission source entity.
   */
  public function getWebformSubmissionSourceEntityId(): int|string|null {
    return $this->webformSubmissionSourceEntityId;
  }

  /**
   * Returns the Webform submission ID.
   *
   * @return int|string|null
   *   The ID of the submission.
   */
  public function getWebformSubmissionId(): int|string|null {
    return $this->webformSubmissionId;
  }

  /**
   * Returns the opt-in mail address.
   *
   * @return string
   *   The opt-in mail address.
   */
  public function getEmailAddress(): string {
    return $this->emailAddress;
  }

}
